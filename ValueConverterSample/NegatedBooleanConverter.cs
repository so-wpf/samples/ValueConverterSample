﻿namespace ValueConverterSample
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    [ValueConversion(typeof(bool), typeof(bool))]
    public sealed class NegatedBooleanConverter : IValueConverter
    {
        /// <summary>
        /// Adding a static field to enable referencing via {x:static local:NegatedBooleanConverter.Default} in xaml
        /// </summary>
        public static readonly NegatedBooleanConverter Default = new NegatedBooleanConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isTrue)
            {
                return !isTrue;
            }

            // returning the raw value to let the binding notify about the error.
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isTrue)
            {
                return !isTrue;
            }

            // returning the raw value to let the binding notify about the error.
            return value;
        }
    }
}